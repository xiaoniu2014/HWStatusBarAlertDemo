//
//  AppDelegate.h
//  HWStatusBarAlertDemo
//
//  Created by hongw on 16/3/13.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

