//
//  ViewController.m
//  HWStatusBarAlertDemo
//
//  Created by hongw on 16/3/13.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import "ViewController.h"
#import "HWStatusBarAlert.h"

@interface ViewController ()
{
    HWStatusBarAlert *_alert;
}


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _alert = [[HWStatusBarAlert alloc] init];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [_alert showWithText:@"淘宝安装成功" style:HWStatusBarAlertStyleSuccess tapBlock:^{
        NSLog(@"淘宝success");
    }];
    
    [_alert showWithText:@"微信安装失败" style:HWStatusBarAlertStyleFailure tapBlock:^{
        NSLog(@"微信failure");
    }];
    
    [_alert showWithText:@"支付宝安装成功" style:HWStatusBarAlertStyleSuccess tapBlock:^{
        NSLog(@"支付宝success");
    }];
}

@end
