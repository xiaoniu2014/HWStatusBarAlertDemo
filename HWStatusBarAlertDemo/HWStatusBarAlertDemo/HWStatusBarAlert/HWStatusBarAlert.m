//
//  HWStatusBarAlert.m
//  HWStatusBarAlertDemo
//
//  Created by hongw on 16/3/13.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import "HWStatusBarAlert.h"

@interface HWTapEvent : NSObject

@property (nonatomic,copy)          void(^tapBlock)();
@property (nonatomic,copy)          NSString                *text;
@property (nonatomic, assign)       HWStatusBarAlertStyle   style;

+ (instancetype)eventWithText:(NSString *)text type:(HWStatusBarAlertStyle)style tapBlock:(void (^)())block;

@end

@implementation HWTapEvent

+ (instancetype)eventWithText:(NSString *)text type:(HWStatusBarAlertStyle)style tapBlock:(void (^)())block{
    HWTapEvent *event = [[super alloc] init];
    event.text = text;
    event.style = style;
    event.tapBlock = block;
    return event;
}

@end

@interface HWStatusBarAlert()
@property (nonatomic,copy)          void(^tapBlock)();
@property (nonatomic,assign)        BOOL                isAnimation;
@property (nonatomic,strong)        NSMutableArray      *tapEvents;
@end

@implementation HWStatusBarAlert
static UIWindow *statusWindow_;
static CGFloat const statusWindow_H = 20.0f;
static double const animationDuration = 0.35f;
static NSTimer *timer_;

//+ (HWStatusBarAlert *)sharedInstance {
//    static dispatch_once_t once;
//    static HWStatusBarAlert *sharedInstance;
//    dispatch_once(&once, ^ {
//        sharedInstance = [[self alloc] init];
//    });
//    return sharedInstance;
//}

- (instancetype)showWithText:(NSString *)text style:(HWStatusBarAlertStyle)style  tapBlock:(void (^)())block{
    // 创建窗口
    HWTapEvent *tapEvent = [HWTapEvent eventWithText:text type:style tapBlock:block];
    [self.tapEvents addObject:tapEvent];
    
    if (!self.isAnimation) {
        self.isAnimation = YES;
        [self starAnimation];
    }
    return self;
}

- (void)starAnimation{
    statusWindow_.hidden = YES;
    statusWindow_ = [[UIWindow alloc]init];
    statusWindow_.backgroundColor = [UIColor blackColor];
    statusWindow_.windowLevel = UIWindowLevelAlert;
    statusWindow_.frame = CGRectMake(0, -statusWindow_H, [UIScreen mainScreen].bounds.size.width, statusWindow_H);
    statusWindow_.hidden = NO;
    // 创建按钮
    HWTapEvent *event = [self.tapEvents objectAtIndex:0];
    self.alertStyle = event.style;
    UIButton *button = [self createButtonWithText:event.text];
    [statusWindow_ addSubview:button];
    
    // 动画
    [UIView animateWithDuration:animationDuration animations:^{
        CGRect frame = statusWindow_.frame;
        frame.origin.y = 0;
        statusWindow_.frame = frame;
    }];
    
    
    
    __weak __typeof(self)weakSelf = self;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [UIView animateWithDuration:animationDuration animations:^{
            statusWindow_.alpha = 0;
        } completion:^(BOOL finished) {
            statusWindow_.hidden = YES;
            statusWindow_ = nil;
            [weakSelf.tapEvents removeObjectAtIndex:0];
            if (weakSelf.tapEvents.count > 0) {
                [weakSelf starAnimation];
            }else{
                weakSelf.isAnimation = NO;
            }
        }];
    });
    
}

- (UIButton *)createButtonWithText:(NSString *)text{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = statusWindow_.bounds;
    [button setTitle:text forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:13];
    [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}


- (void)buttonClick:(UIButton *)button{
    button.enabled = NO;
    if (self.tapEvents.count > 0) {
        HWTapEvent *event = self.tapEvents[0];
        if (event.tapBlock) {
            event.tapBlock();
        }
    }
}

#pragma mark - setter/getter
- (void)setBackgroundColor:(UIColor *)backgroundColor{
    if (_backgroundColor != backgroundColor) {
        _backgroundColor = backgroundColor;
        statusWindow_.backgroundColor = backgroundColor;
    }
}

- (NSMutableArray *)tapEvents{
    if (!_tapEvents) {
        _tapEvents = [[NSMutableArray alloc] init];
    }
    return _tapEvents;
}

- (void)setAlertStyle:(HWStatusBarAlertStyle)alertStyle{
    if (_alertStyle != alertStyle) {
        _alertStyle = alertStyle;
        UIColor *backgroundColor = [UIColor blackColor];
        switch (alertStyle) {
            case HWStatusBarAlertStyleNone:
                
                break;
            case HWStatusBarAlertStyleSuccess:
                backgroundColor = [UIColor darkGrayColor];
                break;
            case HWStatusBarAlertStyleFailure:
                backgroundColor = [UIColor redColor];
                break;
                
            default:
                break;
        }
        statusWindow_.backgroundColor = backgroundColor;
    }
}



@end
