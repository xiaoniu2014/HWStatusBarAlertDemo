//
//  HWStatusBarAlert.h
//  HWStatusBarAlertDemo
//
//  Created by hongw on 16/3/13.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, HWStatusBarAlertStyle) {
    HWStatusBarAlertStyleNone,
    HWStatusBarAlertStyleSuccess,
    HWStatusBarAlertStyleFailure
};

@interface HWStatusBarAlert : NSObject

@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, assign) HWStatusBarAlertStyle alertStyle;

//+ (HWStatusBarAlert*)sharedInstance;
- (instancetype)showWithText:(NSString *)text style:(HWStatusBarAlertStyle)style  tapBlock:(void (^)())block;


@end
