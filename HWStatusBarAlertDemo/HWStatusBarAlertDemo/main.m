//
//  main.m
//  HWStatusBarAlertDemo
//
//  Created by hongw on 16/3/13.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
